/* Copyright © 2017 Tylor Allison */

using UnityEngine;
using Random = UnityEngine.Random;
using System.Collections.Generic;

using TyMesh;

namespace TyTest {
    public class TestMultiMesh: MeshTest {

        public override void Start() {
            // mesh test setup
            Setup();
            
            // create meshdata
            var meshData = new MeshDataExpanding(20);
            for (var i=0; i<20; i++) {
                var origin = new Vector3(
                    Random.Range(0,20),
                    Random.Range(0,20),
                    Random.Range(0,20));
                TestCubeMaker.Build(origin, 2f, MaterialId.FromColor32(Color.green), meshData);
            }

            // create mesh converter
            var meshConverter = new MeshConverterColor();

            // create controller
            meshController.Setup(
                goPool,
                meshConverter
            );

            meshController.AssignMesh(Vector3.zero, meshData, "test");
        }
    }
}
