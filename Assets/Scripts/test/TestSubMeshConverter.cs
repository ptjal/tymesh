/* Copyright © 2017 Tylor Allison */
using UnityEngine;
using System.Collections.Generic;

using TyMesh;

namespace TyTest {

    public class TestSubMeshConverter: MeshTest {

        Dictionary<uint, Material> materialMap;
        void AssignMaterial(uint materialId, Color color) {
            var material = new Material(Shader.Find("Standard"));
            material.color = color;
            materialMap[materialId] = material;
        }
        Material GetMaterial(uint materialId) {
            return materialMap[materialId];
        }

        public override void Start() {
            // mesh test setup
            Setup();

            materialMap = new Dictionary<uint,Material>();

            // create meshdata
            var meshData = new MeshDataExpanding(96);

            TestCubeMaker.Build(Vector3.zero, 5f, 0, meshData);
            TestCubeMaker.Build(new Vector3(2.5f,2.5f,2.5f), 5f, 1, meshData);
            TestCubeMaker.Build(new Vector3(-2.5f,-2.5f,-2.5f), 5f, 2, meshData);

            AssignMaterial(0, Color.red);
            AssignMaterial(1, Color.green);
            AssignMaterial(2, Color.blue);

            // create mesh converter
            var meshConverter = new MeshConverterSubmesh(GetMaterial);

            // create controller
            meshController.Setup(
                goPool,
                meshConverter
            );
            meshController.AssignMesh(Vector3.zero, meshData, "test");
        }
    }
}
