/* Copyright © 2017 Tylor Allison */

using UnityEngine;
using System.Collections.Generic;

using TyMesh;

namespace TyTest {
    public class MeshTest: GenericTest {
        protected GameObjectPool goPool;
        protected MeshController meshController;
        protected void Setup() {
            var mcGO = new GameObject("MeshController");
            objectCache.Add(mcGO);
            meshController = mcGO.AddComponent<MeshController>();
            goPool = new GameObjectPool("test pool", new GameObjectFactory());
        }
        public override void Stop() {
            if (goPool != null) {
                goPool.Clear();
            }
            base.Stop();
        }
    }

    public class TestColorConverter: MeshTest {

        public override void Start() {
            // mesh test setup
            Setup();

            // create meshdata
            var meshData = new MeshDataExpanding(96);
            TestCubeMaker.Build(Vector3.zero, 5f, MaterialId.FromColor32(Color.red), meshData);

            var origin = new Vector3(0,10,0);
            var size = 5f;
            meshData.AddFace(new Vector3(origin.x, origin.y, origin.z),
                             MaterialId.FromColor32(Color.red),
                             new Vector3(origin.x, origin.y, origin.z+size),
                             MaterialId.FromColor32(Color.green),
                             new Vector3(origin.x+size,origin.y,origin.z+size),
                             MaterialId.FromColor32(Color.blue),
                             new Vector3(origin.x+size,origin.y,origin.z),
                             MaterialId.FromColor32(Color.yellow)
            );

            // create mesh converter
            var meshConverter = new MeshConverterColor();

            // create controller
            meshController.Setup(
                goPool,
                meshConverter
            );
            meshController.AssignMesh(Vector3.zero, meshData, "test");
        }
    }
}
