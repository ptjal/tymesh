/* Copyright © 2017 Tylor Allison */

using UnityEngine;
using TyMesh;

public static class TestCubeMaker {
    public static void Build(Vector3 origin, float size, uint materialId, IMeshData meshData) {
        meshData.AddFace(materialId,
                         new Vector3(origin.x, origin.y,origin.z),
                         new Vector3(origin.x, origin.y+size,origin.z),
                         new Vector3(origin.x+size,origin.y+size,origin.z),
                         new Vector3(origin.x+size,origin.y,origin.z));
        meshData.AddFace(materialId,
                         new Vector3(origin.x, origin.y,origin.z),
                         new Vector3(origin.x, origin.y,origin.z+size),
                         new Vector3(origin.x,origin.y+size,origin.z+size),
                         new Vector3(origin.x,origin.y+size,origin.z));
        meshData.AddFace(materialId,
                         new Vector3(origin.x, origin.y,origin.z),
                         new Vector3(origin.x+size, origin.y,origin.z),
                         new Vector3(origin.x+size,origin.y,origin.z+size),
                         new Vector3(origin.x,origin.y,origin.z+size));

        meshData.AddFace(materialId,
                         new Vector3(origin.x+size, origin.y+size, origin.z+size),
                         new Vector3(origin.x+size, origin.y+size, origin.z),
                         new Vector3(origin.x, origin.y+size, origin.z),
                         new Vector3(origin.x, origin.y+size, origin.z+size));
        meshData.AddFace(materialId,
                         new Vector3(origin.x+size, origin.y+size, origin.z+size),
                         new Vector3(origin.x+size, origin.y, origin.z+size),
                         new Vector3(origin.x+size, origin.y, origin.z),
                         new Vector3(origin.x+size, origin.y+size, origin.z));
        meshData.AddFace(materialId,
                         new Vector3(origin.x+size, origin.y+size, origin.z+size),
                         new Vector3(origin.x, origin.y+size, origin.z+size),
                         new Vector3(origin.x, origin.y, origin.z+size),
                         new Vector3(origin.x+size, origin.y, origin.z+size));

    }
}
