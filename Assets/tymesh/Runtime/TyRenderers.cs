﻿/* Copyright © 2018 Tylor Allison. All rights reserved. */

using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.EventSystems;

namespace TyMesh {

    public interface ITyRenderer {
        string name{get;}
        void Render();
        void Clear();
        void Flush();
    }

    public delegate void ActionFcn();
    public delegate void DrawFaceFcn(Color color, params Vector3[] vertices);

    public class TyRenderer : ITyRenderer {
        ActionFcn onFlushCallback;
        public string name{get{return _name;}}
        protected string _name;
        protected MeshController meshController;
        public IMeshData meshdata;
        public TyRenderer(
            string name,
            List<GameObject> objectCache,
            GameObjectPool goPool
        ) {
            this._name = name;
            // setup meshController
            var mcGO = (GameObject) goPool.Produce();
            mcGO.name = name;
            objectCache.Add(mcGO);
            meshController = mcGO.AddComponent<MeshController>();
            var meshConverter = new MeshConverterColor();
            meshController.Setup(goPool, meshConverter);
            // setup mesh data storage/shapes
            meshdata = new MeshDataExpanding();
            onFlushCallback = () => {
                //Debug.Log("releasing: " + mcGO);
                goPool.Release(mcGO);
            };
        }
        public virtual void Render() {
            if (meshController != null) {
                meshController.Clear();
                meshController.AssignMesh(Vector3.zero, meshdata, name);
            }
        }
        public virtual void Clear() {
            if (meshController != null) {
                meshController.Clear();
            }
            if (meshdata != null) {
                meshdata.Clear();
            }
        }
        public virtual void Flush() {
            // reclaim resources
            if (meshController != null) {
                meshController.Clear();
            }
            if (onFlushCallback != null) {
                onFlushCallback();
                onFlushCallback = null;
            }
        }

        public void DrawFace(
            Color color,
            params Vector3[] vertices
        ) {
            if (vertices.Length == 3) {
                meshdata.AddFace(MaterialId.FromColor32(color), vertices[0], vertices[1], vertices[2]);
            } else if (vertices.Length == 4) {
                meshdata.AddFace(MaterialId.FromColor32(color), vertices[0], vertices[1], vertices[2], vertices[3]);
            }
        }

        /*
        public void DrawFace(
            uint color,
            Vector3 v0,
            Vector3 v1,
            Vector3 v2,
            Vector3 v3
        ) {
            meshdata.AddFace(color, v0, v1, v2, v3);
        }
        */

        public static DrawFaceFcn GetDrawFace(TyShapeRenderer renderer) {
            if (renderer != null) {
                return renderer.DrawFace;
            }
            return null;
        }

    }

    public class TyShapeRenderer : TyRenderer {
        SimpleShapes shapes;
        public TyShapeRenderer(
            string name,
            List<GameObject> objectCache,
            GameObjectPool goPool
        ) : base(name, objectCache, goPool) {
            shapes = new SimpleShapes(meshdata);
        }
        public void DrawLine(Vector3 start, Vector3 end, Color color) {
            shapes.DrawLine(start, end, color);
        }
        public void DrawPoint(Vector3 point, Color color) {
            shapes.DrawSphere(point, color);
        }
        public void DrawCircle(Vector3 center, Vector3 normal, float radius, Color color) {
            shapes.DrawCircle(center, normal, radius, color);
        }
        public static DrawLineFcn GetDrawLine(TyShapeRenderer renderer) {
            if (renderer != null) {
                return renderer.DrawLine;
            }
            return null;
        }
        public static DrawPointFcn GetDrawPoint(TyShapeRenderer renderer) {
            if (renderer != null) {
                return renderer.DrawPoint;
            }
            return null;
        }
        public static DrawCircleFcn GetDrawCircle(TyShapeRenderer renderer) {
            if (renderer != null) {
                return renderer.DrawCircle;
            }
            return null;
        }
    }

    public class TyRendererSet : ITyRenderer {
        Dictionary<string,ITyRenderer> renderers;
        public string name{get{return "renderer set";}}

        public TyRendererSet() {
            renderers = new Dictionary<string,ITyRenderer>();
        }

        public void Add(string key, ITyRenderer renderer) {
            renderers[key] = renderer;
        }
        public void Remove(string key) {
            renderers.Remove(key);
        }
        public ITyRenderer Get(string key) {
            ITyRenderer renderer = null;
            renderers.TryGetValue(key, out renderer);
            return renderer;
        }

        public void Render() {
            foreach(var renderer in renderers.Values) {
                renderer.Render();
            }
        }

        public void Clear() {
            // clear the render data
            foreach(var renderer in renderers.Values) {
                Debug.Log("clearing: " + renderer.name);
                renderer.Clear();
            }
        }

        public virtual void Flush() {
            // flush renderers
            foreach(var renderer in renderers.Values) {
                //Debug.Log("flushing: " + renderer);
                renderer.Flush();
            }
            // flush the list of renderers
            renderers.Clear();
        }
    }
}
