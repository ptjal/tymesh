﻿/* Copyright © 2017 Tylor Allison */

using UnityEngine;
using System;
using System.Text;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace TyMesh {
    public delegate void DrawPointFcn(Vector3 point, Color color);
    public delegate void DrawLineFcn(Vector3 start, Vector3 end, Color color);
    public delegate void DrawCircleFcn(Vector3 center, Vector3 normal, float radius, Color color);

    public class SimpleShapes {
        public IMeshData meshData;
        public float width;
        public float dotWidth;
        public int circleSegments;
        public SimpleShapes(IMeshData meshData, float width=.05f, int circleSegments=16) {
            this.meshData = meshData;
            this.width = width;
            this.dotWidth = width * 2;
            this.circleSegments = circleSegments;
        }
        public void DrawLine(Vector3 p1, Vector3 p2, Color color, float width=0f) {
            var localWidth = (width == 0f) ? this.width : width;
            CylinderShape.AddToMesh(
                meshData,
                MaterialId.FromColor32(color),
                localWidth,
                p1,
                p2);
        }
        public void DrawLine(Vector3 p1, Vector3 p2, uint materialId, float width=0f) {
            var localWidth = (width == 0f) ? this.width : width;
            CylinderShape.AddToMesh(
                meshData,
                materialId,
                localWidth,
                p1,
                p2);
        }

        public void DrawCircle(Vector3 center, Vector3 normal, float radius, Color color, int segments=0, float width=0f) {
            DrawCircle(center, normal, radius, MaterialId.FromColor32(color), segments, width);
        }

        public void DrawCircle(Vector3 center, Vector3 normal, float radius, uint materialID, int segments=0, float width=0f) {
            var localSegments = (segments == 0) ? this.circleSegments : segments;
            var localWidth = (width == 0f) ? this.width : width;
            // angle step ... angle between each segment
            var angleStep = 2*Mathf.PI/localSegments;
            // rotation ... from up to normal
            var rotation = Quaternion.FromToRotation(Vector3.up, normal);
            // iterate through segments
            Vector3 lastVertex = Vector3.zero;
            var angle = 0f;
            for (int i=0; i<=localSegments; i++, angle += angleStep) {
                var vertex = new Vector3(radius * Mathf.Cos(angle), 0, radius * Mathf.Sin(angle));
                if (i>0) {
                    DrawLine(center+rotation*lastVertex, center+rotation*vertex, materialID, localWidth);
                }
                lastVertex = vertex;
            }
        }

        public void DrawDisk(Vector3 center, Vector3 normal, float radius, Color color, int segments=0, float width=0f) {
            DrawDisk(center, normal, radius, MaterialId.FromColor32(color), segments, width);
        }

        public void DrawDisk(Vector3 center, Vector3 normal, float radius, uint materialID, int segments=0, float width=0f) {
            var localSegments = (segments == 0) ? this.circleSegments : segments;
            var localWidth = (width == 0f) ? this.width : width;
            // angle step ... angle between each segment
            var angleStep = 2*Mathf.PI/localSegments;
            // rotation ... from up to normal
            var rotation = Quaternion.FromToRotation(Vector3.up, normal);
            // iterate through segments
            Vector3 lastVertex = Vector3.zero;
            var voffset = new Vector3(0, localWidth/2f, 0);
            var angle = 0f;
            for (var i=0; i<=localSegments; i++, angle += angleStep) {
                var vertex = new Vector3(radius * Mathf.Cos(angle), 0, radius * Mathf.Sin(angle));
                if (i>0) {
                    // edge of disk
                    DrawLine(center+rotation*lastVertex, center+rotation*vertex, materialID, localWidth);
                    // top of disk
                    meshData.AddFace(materialID,
                        center+rotation*(lastVertex+voffset),
                        rotation*(center+voffset),
                        center+rotation*(vertex+voffset)
                    );
                    // bottom of disk
                    meshData.AddFace(materialID,
                        center+rotation*(vertex-voffset),
                        rotation*(center-voffset),
                        center+rotation*(lastVertex-voffset)
                    );
                }
                lastVertex = vertex;
            }
        }

        public void DrawSphere(Vector3 p1, Color color, float width=0f) {
            var localWidth = (width == 0f) ? this.dotWidth: width;
            IcosahedronShape.AddToMesh(
                meshData,
                MaterialId.FromColor32(color),
                p1,
                localWidth);
        }
        public void DrawSphere(Vector3 p1, uint materialId, float width=0f) {
            var localWidth = (width == 0f) ? this.dotWidth: width;
            IcosahedronShape.AddToMesh(
                meshData,
                materialId,
                p1,
                localWidth);
        }
    }
}
