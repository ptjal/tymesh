﻿/* Copyright © 2017 Tylor Allison */

using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

namespace TyMesh {

    public delegate Material MaterialMapFunction(uint materialId);

    // ========================================================================
    /// <summary>
    /// Class to quickly manage mesh data, allowing for incremental build up of mesh
    /// and eventual conversion to Unity Mesh data.
    /// </summary>
    public class MeshConverterSubmesh: MeshConverter {
        MaterialMapFunction materialMapFunction;

        public MeshConverterSubmesh(MaterialMapFunction materialMapFunction) {
            this.materialMapFunction = materialMapFunction;
        }

        /// <summary>
        /// Convert the mesh to a unity mesh and extract the material IDs.
        /// For now, this will break each different material into a submesh and returns
        /// a corresponding list of materials for each submesh.
        /// </summary>
        public override void Convert(
            GameObject parentGo,
            IMeshData meshData,
            IResourcePool<GameObject> goPool
        ) {

            if (meshData.empty) return;

            var meshIndex = 0;
            foreach (var storage in meshData.Values()) {

                // build up submeshes of triangle data based on material
                uint[] materialIndices;
                var materialMeshes = new Dictionary<uint, List<int>>();
                for (var i=0; i<storage.triangles.Count; i+=3) {
                    List<int> subTriangles;
                    var vertexIndex = storage.triangles[i];
                    var materialId = storage.materials[vertexIndex];
                    if (!materialMeshes.TryGetValue(materialId, out subTriangles)) {
                        subTriangles = new List<int>();
                        materialMeshes.Add(materialId, subTriangles);
                    }
                    subTriangles.Add(storage.triangles[i]);
                    subTriangles.Add(storage.triangles[i+1]);
                    subTriangles.Add(storage.triangles[i+2]);
                }
                var mesh = new Mesh {
                    vertices = storage.vertices.ToArray(),
                    uv = storage.uvs.ToArray(),
                    subMeshCount = materialMeshes.Count
                };

                // add each material triangles/faces as separate submesh
                var submeshIndex = 0;
                foreach (var pair in materialMeshes) {
                    var subTriangles = pair.Value.ToArray();
                    mesh.SetTriangles(subTriangles, submeshIndex++);
                }
                mesh.RecalculateNormals();
                materialIndices = materialMeshes.Keys.ToArray();

                // create new gameobject for mesh
                var meshObject = goPool.Produce();
                var filter = meshObject.GetComponent<MeshFilter>();
                var collider = meshObject.GetComponent<MeshCollider>();
                var renderer = meshObject.GetComponent<MeshRenderer>();
                filter.mesh.Clear();
                filter.sharedMesh = mesh;
                var materials = new Material[materialMeshes.Count];
                for (var i=0; i<materialMeshes.Count; i++) {
                    materials[i] = materialMapFunction(materialIndices[i]);
                }
                renderer.sharedMaterials = materials;
                if (includeCollider) {
                    collider.sharedMesh = filter.sharedMesh;
                }
                meshIndex++;
            }
        }

    }
}
