/* Copyright © 2017 Tylor Allison */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace TyMesh {

    // create mesh object pool implementation, modified for mesh controller use
    // this implementation will return the mesh controller GO as the first pool object
    // then pull from the pool
    class MeshObjectPool: IResourcePool<GameObject> {

        [System.Serializable]
        public class ResourcePoolEvent : UnityEvent<GameObject>
        {
        }

        IResourcePool<GameObject> meshObjectPool;
        MeshController meshController;
        List<GameObject> meshGOs;
        bool includeCollider;
        protected UnityEvent<GameObject> _onProduce;
        public virtual UnityEvent<GameObject> onProduce {
            get {
                return _onProduce;
            }
        }

        public string meshName;

        public MeshObjectPool(
            IResourcePool<GameObject> meshObjectPool,
            MeshController meshController,
            bool includeCollider
        ) {
            this.meshObjectPool = meshObjectPool;
            this.meshController = meshController;
            this.includeCollider = includeCollider;
            _onProduce = new ResourcePoolEvent();
            meshGOs = new List<GameObject>();
        }

        public GameObject Produce() {
            GameObject meshGO = null;
            if (!meshController.meshUsed) {
                meshController.meshUsed = true;
                meshGO = meshController.gameObject;
                meshGO.name = String.Format("{0}", meshName);
            } else {
                meshGO = meshObjectPool.Produce();
                meshGO.transform.position = Vector3.zero;
                meshGO.name = String.Format("{0}.{1}", meshName, meshGOs.Count);
                meshGOs.Add(meshGO);
                meshGO.transform.SetParent(meshController.gameObject.transform, false);

            }

            // add required mesh components
            if (null == meshGO.GetComponent<MeshFilter>()) {
                meshGO.AddComponent<MeshFilter>();
            }
            if (includeCollider &&
                null == meshGO.GetComponent<MeshCollider>()) {
                meshGO.AddComponent<MeshCollider>();
            }
            if (null == meshGO.GetComponent<MeshRenderer>()) {
                meshGO.AddComponent<MeshRenderer>();
            }
            _onProduce.Invoke(meshGO);
            return meshGO;
        }

        public void Release(GameObject go) {
            if (go == meshController.gameObject) {
                meshController.meshUsed = false;
            } else {
                if (meshGOs.Contains(go)) {
                    meshGOs.Remove(go);
                }
                meshObjectPool.Release(go);
            }
        }

        public void Clear() {
            meshController.meshUsed = false;
            for (var i=0; i<meshGOs.Count; i++) {
                meshObjectPool.Release(meshGOs[i]);
            }
            meshGOs.Clear();
        }
    }

    public class MeshController : MonoBehaviour, IPointerClickHandler {
        public bool meshUsed = false;
        public IMeshConverter meshConverter;
        public IResourcePool<GameObject> goPool;

        #region IPointerClickHandler implementation
        public void OnPointerClick (PointerEventData eventData) {
            //Debug.Log(String.Format("OnPointerClick for gameObject: {0}, pointerData: {1} ", this, eventData));
            //var args = new RenderEventArgs();
            //args.pointerData = eventData;
            //args.meshData = this.meshData;
            //EventManager.instance.InvokeHandler(RenderEventKind.pointerClick, args);
        }
        #endregion

        void Awake () {
            //meshConverter = new MeshConverterColor();
        }

        public void Setup(
            IResourcePool<GameObject> goPool,
            IMeshConverter meshConverter
        ) {
            // assign conversion implementation
            this.meshConverter = meshConverter;
            // assign gameobject resource pool implementation
            this.goPool = new MeshObjectPool(goPool, this, true);
        }

        public void AssignMesh(
            Vector3 position,
            IMeshData meshData,
            String name=null
        ) {
            ((MeshObjectPool) goPool).meshName = name;
            meshConverter.Convert(gameObject, meshData, goPool);
        }

        public void Clear() {
            if (goPool != null) {
                goPool.Clear();
            }
        }

        public void SetActive(bool active) {
            gameObject.SetActive(active);
        }

        void OnDestroy() {
            Clear();
        }

    }

}
