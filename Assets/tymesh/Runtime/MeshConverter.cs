﻿/* Copyright © 2017 Tylor Allison */

using UnityEngine;

namespace TyMesh {

    // ========================================================================
    /// <summary>
    /// Class to quickly manage mesh data, allowing for incremental build up of mesh
    /// and eventual conversion to Unity Mesh data.
    /// </summary>
    public class MeshConverter: IMeshConverter {
        public bool includeCollider = true;

        public virtual void Convert(
            GameObject parentGo,
            IMeshData meshData,
            IResourcePool<GameObject> goPool
        ) {
        }
    }
}
