/* Copyright © 2017 Tylor Allison */

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
// FIXME: remove
using System.Linq;

namespace TyMesh {
    public class GameObjectPool : ResourcePool<GameObject> {
        protected string name;
        protected GameObject root;

        public GameObjectPool(
            string name,
            IFactory<GameObject> factory
        ) : base(factory) {
            this.name = name;
        }

        void SetParent(GameObject go) {
            if (root == null) {
                root = factory.Produce();
                root.name = name;
            }
            go.transform.SetParent(root.transform, false);
        }

        public override GameObject Produce() {
            var poolObject = base.Produce();
            poolObject.SetActive(true);
            SetParent(poolObject);
            return poolObject;
        }

        public override void Release(
            GameObject poolObject
        ) {
            if (poolObject != null) {
                poolObject.name = "pool unused";
                poolObject.SetActive(false);
                SetParent(poolObject);
                base.Release(poolObject);
            }
        }

        public override void Clear() {
            lock (available) {
                foreach (var go in available) {
                    MonoBehaviour.Destroy(go);
                }
                foreach (var go in inUse) {
                    MonoBehaviour.Destroy(go);
                }
                available.Clear();
                inUse.Clear();
                if (root != null) {
                    MonoBehaviour.Destroy(root);
                    root = null;
                }
            }
        }

        public void Dump() {
            Debug.Log("available: " + System.String.Join("\n", available.Select(v => v.ToString()).ToArray()));
            Debug.Log("inUse: " + System.String.Join("\n", inUse.Select(v => v.ToString()).ToArray()));
        }
    }
}
