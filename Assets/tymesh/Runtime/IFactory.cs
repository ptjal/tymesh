/* Copyright © 2017 Tylor Allison */
using UnityEngine;
using UnityEngine.Events;

namespace TyMesh {
    public interface IFactory<T> {
        T Produce();
        UnityEvent<T> onProduce {get;}
    }
}
