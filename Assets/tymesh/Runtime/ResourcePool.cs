/* Copyright © 2017 Tylor Allison */

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TyMesh {
    public class ResourcePool<T> : IResourcePool<T> {

        [System.Serializable]
        public class ResourcePoolEvent : UnityEvent<T>
        {
        }

        protected List<T> available;
        protected List<T> inUse;
        protected IFactory<T> factory;

        protected UnityEvent<T> _onProduce;
        public virtual UnityEvent<T> onProduce {
            get {
                return _onProduce;
            }
        }

        public ResourcePool(IFactory<T> factory) {
            _onProduce = new ResourcePoolEvent();
            available = new List<T>();
            inUse = new List<T>();
            this.factory = factory;
        }

        public virtual T Produce() {
            T poolObject;
            lock(available) {
                if (available.Count != 0) {
                    poolObject = available[0];
                    inUse.Add(poolObject);
                    available.RemoveAt(0);
                } else {
                    poolObject = factory.Produce();
                    inUse.Add(poolObject);
                }
            }
            _onProduce.Invoke(poolObject);
            return poolObject;
        }

        public virtual void Release(T poolObject) {
            lock (available) {
                available.Add(poolObject);
                inUse.Remove(poolObject);
            }
        }

        public virtual void Clear() {
        }
    }
}
