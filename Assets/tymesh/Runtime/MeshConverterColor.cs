﻿/* Copyright © 2017 Tylor Allison */

using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

namespace TyMesh {

    // ========================================================================
    /// <summary>
    /// Class to quickly manage mesh data, allowing for incremental build up of mesh
    /// and eventual conversion to Unity Mesh data.
    /// </summary>
    public class MeshConverterColor: MeshConverter {

        /// <summary>
        /// Convert the mesh to a unity mesh and extract the material IDs.
        /// For now, this will break each different material into a submesh and returns
        /// a corresponding list of materials for each submesh.
        /// </summary>
        public override void Convert(
            GameObject parentGo,
            IMeshData meshData,
            IResourcePool<GameObject> goPool
        ) {
            // check for empty
            if (meshData.empty) return;

            // iterate over data sets
            var meshIndex = 0;
            foreach (var storage in meshData.Values()) {
                // create color32 array
                var colors = new Color32[storage.vertices.Count];
                for (var i=0; i<storage.vertices.Count; i++) {
                    colors[i] = MaterialId.ToColor32(storage.materials[i]);
                }
                var mesh = new Mesh {
                    vertices = storage.vertices.ToArray(),
                    uv = storage.uvs.ToArray(),
                    triangles = storage.triangles.ToArray(),
                    colors32 = colors
                };
                mesh.RecalculateNormals();

                var meshObject = goPool.Produce();
                var filter = meshObject.GetComponent<MeshFilter>();
                var collider = meshObject.GetComponent<MeshCollider>();
                var renderer = meshObject.GetComponent<MeshRenderer>();
                filter.mesh.Clear();
                filter.sharedMesh = mesh;
                if (renderer.sharedMaterial == null) {
                    renderer.sharedMaterial = new Material(Shader.Find("Custom/StandardVertex"));
                }
                if (includeCollider) {
                    collider.sharedMesh = filter.sharedMesh;
                }
                meshIndex++;
            }

        }

    }
}
