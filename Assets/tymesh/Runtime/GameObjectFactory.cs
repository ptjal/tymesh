/* Copyright © 2017 Tylor Allison */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace TyMesh {

    [System.Serializable]
    public class GameObjectEvent : UnityEvent<GameObject>
    {
    }

    public class GameObjectFactory: IFactory<GameObject> {
        public UnityEvent<GameObject> _onProduce;
        public UnityEvent<GameObject> onProduce {
            get {
                return _onProduce;
            }
        }

        public GameObjectFactory() {
            _onProduce = new GameObjectEvent();
        }

        public GameObject Produce() {
            var go = new GameObject();
            _onProduce.Invoke(go);
            return go;
        }
    }
}
