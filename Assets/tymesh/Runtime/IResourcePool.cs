/* Copyright © 2017 Tylor Allison */
using UnityEngine;
using UnityEngine.Events;

namespace TyMesh {
    public interface IResourcePool<T> {
        T Produce();
        void Release(T obj);
        UnityEvent<T> onProduce {get;}
        void Clear();
    }
}
