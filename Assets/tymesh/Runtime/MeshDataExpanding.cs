﻿/* Copyright © 2017 Tylor Allison */

using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

namespace TyMesh {

    // ========================================================================
    /// <summary>
    /// Class to quickly manage mesh data, allowing for incremental build up of mesh
    /// and eventual conversion to Unity Mesh data.
    /// </summary>
    public class MeshDataExpanding: IMeshData {

        // ------------------------------------------------------
        // INSTANCE VARIABLES
        int maxVertices;
        List<MeshData> meshDatas;
        MeshData current;

        // ------------------------------------------------------
        // PROPERTIES
        public int vertexCount {
            get {
                var count = 0;
                foreach (var meshData in meshDatas) {
                    count += meshData.vertexCount;
                }
                return count;
            }
        }

        public bool empty {
            get {
                return vertexCount == 0;
            }
        }

        // ------------------------------------------------------
        // CONSTRUCTORS
        public MeshDataExpanding(int maxVertices=65000) {
            this.maxVertices = maxVertices;
            this.meshDatas = new List<MeshData>();
            AddStorage();
        }

        // ------------------------------------------------------
        // INSTANCE METHODS

        void
        AddStorage() {
            var meshData = new MeshData();
            meshDatas.Add(meshData);
            current = meshData;
        }

        public IEnumerable<MeshDataStorage> Values() {
            foreach (var meshData in meshDatas) {
                foreach (var storage in meshData.Values()) {
                    yield return storage;
                }
            }
        }

        /// <summary>
        /// String representation of class.
        /// </summary>
        public override string ToString() {
            return "vertices: " + vertexCount +
                   "\nmeshDatas: " + meshDatas.Count;
        }

        /// <summary>
        /// Add face with given material index to mesh data, where a face is referenced by the
        /// given set of vertices.  Set of vertices is broken into triangles.
        /// Update UVs based on number of added triangles
        /// </summary>
        public void AddFace(
            uint materialId,
            Vector3 v0,
            Vector3 v1,
            Vector3 v2
        ) {
            if (current.vertexCount + 3 > maxVertices) {
                AddStorage();
            }
            current.AddFace(materialId, v0, v1, v2);
        }

        public void AddFace(
            Vector3 v0,
            uint mat0,
            Vector3 v1,
            uint mat1,
            Vector3 v2,
            uint mat2
        ) {
            if (current.vertexCount + 3 > maxVertices) {
                AddStorage();
            }
            current.AddFace(v0, mat0, v1, mat1, v2, mat2);
        }

        public void AddFace(
            uint materialId,
            Vector3 v0,
            Vector3 v1,
            Vector3 v2,
            Vector3 v3
        ) {
            if (current.vertexCount + 4 > maxVertices) {
                AddStorage();
            }
            current.AddFace(materialId, v0, v1, v2, v3);
        }

        public void AddFace(
            Vector3 v0,
            uint mat0,
            Vector3 v1,
            uint mat1,
            Vector3 v2,
            uint mat2,
            Vector3 v3,
            uint mat3
        ) {
            if (current.vertexCount + 4 > maxVertices) {
                AddStorage();
            }
            current.AddFace(v0, mat0, v1, mat1, v2, mat2, v3, mat3);
        }

        public void Clear() {
            this.meshDatas = new List<MeshData>();
            AddStorage();
        }
    }
}
