﻿/* Copyright © 2017 Tylor Allison */

using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

namespace TyMesh {

    public interface IMeshData {

        // ------------------------------------------------------
        // PROPERTIES
        int vertexCount { get; }
        bool empty { get; }

        // ------------------------------------------------------
        // INSTANCE METHODS
        void AddFace(
            uint materialId,
            Vector3 v0,
            Vector3 v1,
            Vector3 v2
        );
        void AddFace(
            Vector3 v0,
            uint mat0,
            Vector3 v1,
            uint mat1,
            Vector3 v2,
            uint mat2
        );
        void AddFace(
            uint materialId,
            Vector3 v0,
            Vector3 v1,
            Vector3 v2,
            Vector3 v3
        );
        void AddFace(
            Vector3 v0,
            uint mat0,
            Vector3 v1,
            uint mat1,
            Vector3 v2,
            uint mat2,
            Vector3 v3,
            uint mat3
        );
        IEnumerable<MeshDataStorage> Values();
        void Clear();
    }
}
