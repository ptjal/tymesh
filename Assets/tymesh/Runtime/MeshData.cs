﻿/* Copyright © 2017 Tylor Allison */

using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

namespace TyMesh {

    static class UvMap {
        // ------------------------------------------------------
        // STATIC VARIABLES
        public static Vector2 [] uv3 = new Vector2[3] {
            new Vector2(1.0f, 1.0f),
            new Vector2(0.0f, 1.0f),
            new Vector2(0.0f, 0.0f),
        };
        public static Vector2 [] uv4 = new Vector2[4] {
            new Vector2(1.0f, 0.0f),
            new Vector2(1.0f, 1.0f),
            new Vector2(0.0f, 1.0f),
            new Vector2(0.0f, 0.0f),
        };
        /*
        // FIXME: I think these are all wrong
        static Vector2 [] uv5 = new Vector2[] {
            new Vector2(-0.1784f, -0.5491f),
            new Vector2(-0.5774f, 0.0000f),
            new Vector2(-0.1784f, 0.5491f),
            new Vector2(0.4671f, 0.3394f),
            new Vector2(0.4671f, -0.3394f),
        };
        static Vector2[] uv6 = new Vector2[] {
               new Vector2(0,0.25f),
               new Vector2(0,0.75f),
               new Vector2(0.5f,1),
               new Vector2(1,0.75f),
               new Vector2(1,0.25f),
               new Vector2(0.5f,0),
        };
        static Vector2 [] uv7 = new Vector2[7] {
            new Vector2(1.0f, 0.0f),
            new Vector2(1.0f, 1.0f),
            new Vector2(0.0f, 1.0f),
            new Vector2(0.0f, 0.0f),
            new Vector2(1.0f, 1.0f),
            new Vector2(0.0f, 1.0f),
            new Vector2(0.0f, 0.0f),
        };
        */
    }

    // ========================================================================
    /// <summary>
    /// Class to quickly manage mesh data, allowing for incremental build up of mesh
    /// and eventual conversion to Unity Mesh data.
    /// </summary>
    public class MeshData: IMeshData {

        // ------------------------------------------------------
        // INSTANCE VARIABLES
        MeshDataStorage storage;

        // ------------------------------------------------------
        // PROPERTIES
        public int vertexCount {
            get {
                return storage.vertices.Count;
            }
        }

        public bool empty {
            get {
                return vertexCount == 0;
            }
        }

        // ------------------------------------------------------
        // CONSTRUCTORS
        public MeshData(int maxVertices=65000) {
            this.storage = new MeshDataStorage();
        }

        // ------------------------------------------------------
        // INSTANCE METHODS

        /// <summary>
        /// String representation of class.
        /// </summary>
        public override string ToString() {
            return "vertices: " + string.Join("\n", storage.vertices.Select(v => v.ToString()).ToArray()) +
                   "\ntriangles: " + string.Join("\n", storage.triangles.Select(v => v.ToString()).ToArray()) +
                   "\nmaterials: " + string.Join("\n", storage.materials.Select(v => v.ToString()).ToArray());
        }

        /// <summary>
        /// Add face with given material index to mesh data, where a face is referenced by the
        /// given set of vertices.  Set of vertices is broken into triangles.
        /// Update UVs based on number of added triangles
        /// </summary>
        public void AddFace(
            uint materialId,
            Vector3 v0,
            Vector3 v1,
            Vector3 v2
        ) {
            var start = vertexCount;

            // update list of vertices and materials per vertex
            storage.vertices.Add(v0);
            storage.vertices.Add(v1);
            storage.vertices.Add(v2);
            storage.materials.Add(materialId);
            storage.materials.Add(materialId);
            storage.materials.Add(materialId);

            // update list of triangles and associated materials
            storage.triangles.Add(start);
            storage.triangles.Add(start+1);
            storage.triangles.Add(start+2);

            // update list of UVs
            storage.uvs.AddRange(UvMap.uv3);
        }

        public void AddFace(
            Vector3 v0,
            uint mat0,
            Vector3 v1,
            uint mat1,
            Vector3 v2,
            uint mat2
        ) {
            var start = vertexCount;

            // update list of vertices and materials per vertex
            storage.vertices.Add(v0);
            storage.vertices.Add(v1);
            storage.vertices.Add(v2);
            storage.materials.Add(mat0);
            storage.materials.Add(mat1);
            storage.materials.Add(mat2);

            // update list of triangles and associated materials
            storage.triangles.Add(start);
            storage.triangles.Add(start+1);
            storage.triangles.Add(start+2);

            // update list of UVs
            storage.uvs.AddRange(UvMap.uv3);
        }

        public void AddFace(
            uint materialId,
            Vector3 v0,
            Vector3 v1,
            Vector3 v2,
            Vector3 v3
        ) {
            var start = vertexCount;

            // update list of vertices
            storage.vertices.Add(v0);
            storage.vertices.Add(v1);
            storage.vertices.Add(v2);
            storage.vertices.Add(v3);
            storage.materials.Add(materialId);
            storage.materials.Add(materialId);
            storage.materials.Add(materialId);
            storage.materials.Add(materialId);

            // update list of triangles and associated materials
            storage.triangles.Add(start);
            storage.triangles.Add(start+1);
            storage.triangles.Add(start+2);
            storage.triangles.Add(start);
            storage.triangles.Add(start+2);
            storage.triangles.Add(start+3);
            // update list of UVs
            storage.uvs.AddRange(UvMap.uv4);
        }

        public void AddFace(
            Vector3 v0,
            uint mat0,
            Vector3 v1,
            uint mat1,
            Vector3 v2,
            uint mat2,
            Vector3 v3,
            uint mat3
        ) {
            var start = vertexCount;

            // update list of vertices
            storage.vertices.Add(v0);
            storage.vertices.Add(v1);
            storage.vertices.Add(v2);
            storage.vertices.Add(v3);
            storage.materials.Add(mat0);
            storage.materials.Add(mat1);
            storage.materials.Add(mat2);
            storage.materials.Add(mat3);

            // update list of triangles and associated materials
            storage.triangles.Add(start);
            storage.triangles.Add(start+1);
            storage.triangles.Add(start+2);
            storage.triangles.Add(start);
            storage.triangles.Add(start+2);
            storage.triangles.Add(start+3);
            // update list of UVs
            storage.uvs.AddRange(UvMap.uv4);
        }

        public IEnumerable<MeshDataStorage> Values() {
            yield return storage;
        }

        public void Clear() {
            storage.Clear();
        }
    }
}
