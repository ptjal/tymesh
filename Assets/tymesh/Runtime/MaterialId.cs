/* Copyright © 2017 Tylor Allison */
using UnityEngine;

namespace TyMesh {

    public static class MaterialId {
        public static uint FromColor32(Color32 color) {
            return ((uint) color.a)<<24 | ((uint) color.r)<<16 | ((uint) color.g)<<8 | color.b;
        }
        public static Color32 ToColor32(uint materialId) {
            return new Color32(
                (byte) (0xFF&(materialId>>16)),
                (byte) (0xFF&(materialId>>8)),
                (byte) (0xFF&(materialId)),
                (byte) (0xFF&(materialId>>24))
            );
        }
    }
}
