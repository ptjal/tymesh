﻿/* Copyright © 2017 Tylor Allison */

using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

namespace TyMesh {

    public class MeshDataStorage {

        // ------------------------------------------------------
        // INSTANCE VARIABLES
        public readonly List<Vector3> vertices;
        public readonly List<Vector2> uvs;
        public readonly List<int> triangles;
        public readonly List<uint> materials;

        // ------------------------------------------------------
        // CONSTRUCTORS
        public MeshDataStorage() {
            this.vertices = new List<Vector3>();
            this.uvs = new List<Vector2>();
            this.triangles = new List<int>();
            this.materials = new List<uint>();
        }

        public void Clear() {
            vertices.Clear();
            uvs.Clear();
            triangles.Clear();
            materials.Clear();
        }
    }
}
