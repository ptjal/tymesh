# tymesh - A Unity package providing methods for dynamic mesh generation

## Design

### MeshData

Three different types of classes are provided.  

* **IMeshData** classes - providing expanding storage for verts, tris, uvs, materials.
* **IMeshConverter** classes - providing for conversion between the IMeshData storage and 
    the Unity Mesh.
* **MeshController** class - a controlling MonoBehaviour script that ties the IMeshData and
    IMeshConverter together

The point of this module is to provide a simple interface for dealing with dynamically-created meshes while at the same time working around some Unity limitations (such as vertice count).

### SimpleShapes

Two different types of classes are provided.  

* Shape classes - cylinder and icosahedron
* **SimpleShapes** class - provides methods for line/dot drawing using TyMesh primitives

A simple way to create meshes w/ lines and dots... wow!

### TyRenderer

Goal is to make some of the Unity mesh handling and rendering a bit more streamlined and 
easy to use.

A renderer class is meant to store a mesh and know how to render it using Unity.  
There's a ITestRenderer interface that defines common interactions with any renderer, including
things like rendering (output to Unity) and clearing/flushing.  Other actions are renderer
specific.

Two renderer implementations are provided.  **TestRenderer** ties meshdata (my internal
structure for dealing with meshes) to the renderer interface.  Useful for simplifying
the handling of anything that uses the meshdata.  **TestShapeRenderer** ties simple
shape primitives (like drawing lines, points, circles, etc.) to the renderer interface.

There's also a **TestRendererSet** that acts a collection of renderers and retains the 
same renderer interface.

## Usage

Here's a quick example, pulled from the TestColorConverter test class included in this module:

    // create controlling game object instantiate mesh controller
    var mcGO = new GameObject("MeshController");
    var meshController = mcGO.AddComponent<MeshController>();

    // create mesh data storage
    var meshData = new MeshDataExpanding(96);

    // Add a face to the mesh
    var materialId = MaterialId.FromColor32(Color.red);
    var origin = Vector3.zero;
    var size = 5f;
    meshData.AddFace(materialId,
                     new Vector3(origin.x, origin.y,origin.z),
                     new Vector3(origin.x, origin.y+size,origin.z),
                     new Vector3(origin.x+size,origin.y+size,origin.z),
                     new Vector3(origin.x+size,origin.y,origin.z));

    // create mesh converter
    var meshConverter = new MeshConverterColor();

    // create controller
    var meshController = CreateMeshController();
    meshController.meshConverter = meshConverter;
    meshController.AssignMesh(Vector3.zero, meshData, "test");

Simple shapes example:

    // create controlling game object instantiate mesh controller
    var mcGO = new GameObject("MeshController");
    var meshController = mcGO.AddComponent<MeshController>();

    // create mesh data storage
    var meshData = new MeshDataExpanding(96);

    // Add shapes to the mesh
    var meshData = new MeshDataExpanding(96);
    var shapes = new SimpleShapes(meshData);
    var v1 = new Vector3(0,0,0);
    var v2 = new Vector3(5,0,0);
    shapes.DrawSphere(v1, Color.red);
    shapes.DrawSphere(v2, Color.red);
    shapes.DrawLine(v1, v2, Color.blue);

    // create mesh converter
    var meshConverter = new MeshConverterColor();

    // create controller
    var meshController = CreateMeshController();
    meshController.meshConverter = meshConverter;
    meshController.AssignMesh(Vector3.zero, meshData, "test");


